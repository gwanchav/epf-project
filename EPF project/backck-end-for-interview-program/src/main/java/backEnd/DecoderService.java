package backEnd;

import io.javalin.Javalin;
import io.javalin.util.FileUtil;
import io.micrometer.core.instrument.util.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DecoderService {
    private static final String fileDIR ="/home/g/backck-end-for-interview-program/src/main/java/org/" ;


    public static final int DEFAULT_PORT = 7000;
    private Javalin server;


    public DecoderService(){
        server = initHttpServer();

    }
    public void stop(){
        server.stop();
    }

    public void run(){
        server.start( DEFAULT_PORT );
    }

    public static void main(String[] args) {
        final DecoderService decoderService = new DecoderService();
        decoderService.run();

    }

    private Javalin initHttpServer() {
        Javalin server =Javalin.create();
        server.before(ctx -> {
            ctx.header("Access-Control-Allow-Origin", "*");
            System.out.println("ggd");
        });
        System.out.println("ggs");
        return server
                .post("/upload/file", ctx -> {
                    File file = new File(fileDIR + ctx.uploadedFile("file").filename());
                    try (FileOutputStream fos = new FileOutputStream(file)){
                        fos.write(ctx.uploadedFile("file").content().readAllBytes());
                    }
                    ctx.html("Upload successful");
                });


    }

}
