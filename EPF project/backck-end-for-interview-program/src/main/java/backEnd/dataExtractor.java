package backEnd;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.poi.ss.usermodel.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class dataExtractor {


    public static void main(String[] argv) {
        info infoObj = new info();
        List<String> months = Arrays.asList("jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct" ,"nov","dec");

        File dir = new File("/home/g/backck-end-for-interview-program/src/main/java/org");
        File[] directoryListing = dir.listFiles();
        if (directoryListing != null){
            for (File child: directoryListing){
                if (child.getAbsoluteFile().toString().endsWith(".xlsx")){
                    try (FileInputStream file = new FileInputStream(new File(child.getAbsolutePath()))) {
                        Workbook workbook = WorkbookFactory.create(file);
                        Sheet sheet = workbook.getSheetAt(0);
                        for (Row row : sheet) {
                            if (row.getCell(0).toString().equals("name :")){
                                infoObj.name = row.getCell(1).toString();
                            }else if(row.getCell(0).toString().equals("surname :")){
                                infoObj.surname = row.getCell(1).toString();
                            }else if(row.getCell(0).toString().equals("date of birth :")){
                                infoObj.surname = row.getCell(1).toString();
                            }else if (months.contains(row.getCell(0).toString())){
                                String income =row.getCell(1).toString().replace(",","").replace("R","");
                                infoObj.income.add(Integer.valueOf(income));
                                String expense =String.valueOf(row.getCell(2)).replace(",","").replace("R","");
                                infoObj.Expenses.add(Integer.valueOf(expense));

                            }

                            System.out.println(row.getCell(0)+ "," +row.getCell(1));

                        }
                        jsonWriter(infoObj);
                    } catch (IOException e) {
                        e.printStackTrace();
                        System.err.println("an error occured: " + e.getMessage());
                    }
                }
            }
        }


    }

    public static void jsonWriter(info infoObj) {

        String filename = infoObj.name+infoObj.surname+infoObj.dateofbirth+".json";
        String fileDir = "/src/main/java/org/json/";


        ObjectMapper mapper = new ObjectMapper();

        try {
            Path path = Path.of(fileDir+filename);
            System.out.println(path);

            if(!Files.exists(path)){
                System.out.println("gg");

                mapper.writeValue(new File(filename), infoObj );
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
